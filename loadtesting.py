import argparse

from collections import Counter

from autobahn.twisted.websocket import WebSocketClientProtocol, WebSocketClientFactory, connectWS
from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks


class ListenerSocketProtocol(WebSocketClientProtocol):
    """
    Essentially the connections do nothing but listen.
    """
    pass


class StatsSocketFactory(WebSocketClientFactory):
    """Tracks failed and lost connections"""

    def __init__(self, *args, **kwargs):
        super(StatsSocketFactory, self).__init__(*args, **kwargs)

        self.lost_connections = 0
        self.failed_connections = 0

        self.failure_reasons = Counter()

    def reset_stats(self):
        self.lost_connections = 0
        self.failed_connections = 0

    protocol = ListenerSocketProtocol

    def clientConnectionLost(self, connector, reason):
        self.lost_connections += 1
        self.failure_reasons[reason.getErrorMessage()] += 1

    def clientConnectionFailed(self, connector, reason):
        self.failed_connections += 1
        self.failure_reasons[reason.getErrorMessage()] += 1


@inlineCallbacks
def load_test(factory, max=1000, min=10, duration=10, buildup="linear"):
    """
    The load test coroutine
    :param factory: Connection factory
    :param loop: Event loop
    :param max: Number of connections to load test under
    :param min: Minimum number of connections at any time
    :param duration: Duration in seconds
    :return: stats
    """

    buildups = {
        "linear": lambda: [reactor.callLater(i*duration/max, lambda: connectWS(factory)) for i in range(max)],
        "none": lambda: [connectWS(factory) for _ in range(max)]
    }

    connect = buildups[buildup]

    print(f"Firing connections to {factory.url}.")
    connect()
    print("Connections fired.")

    def done():
        reactor.stop()
        print(f"Failed connections: {factory.failed_connections}; Lost connections: {factory.lost_connections}")
        print(f"Failure reasons: {factory.failure_reasons}")

    yield reactor.callLater(duration, done)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str)
    parser.add_argument('--max', type=int)
    parser.add_argument('--min', type=int)
    parser.add_argument('--duration', type=int)

    parser.add_argument('--buildup', type=str, choices=['linear', 'none'], default='linear')

    args = parser.parse_args()

    path = getattr(args, 'path', 'ws://localhost:51955/api/WsServer')

    factory = StatsSocketFactory(path)

    load_test(factory, max=args.max, min=args.min, duration=args.duration)

    reactor.run()


if __name__ == "__main__":
    main()
